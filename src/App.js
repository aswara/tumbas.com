import React, { Component } from 'react';
import Login from './home/Login';
import Home from './home/Home';
import SignUp from './home/SignUp';
import { Redirect, Route, Link, Switch, withRouter  } from 'react-router-dom';
import { dbfirebase } from './firebase';
import { connect } from 'react-redux';
import { logUser } from './actions';
import Navbar from './home/Navbar';
import Admin from './admin/Admin';
import Koleksi from './home/Koleksi';
import Produk from './home/Produk';
import Footer from './home/Footer';


class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            akun : null,
        }
    }

    componentDidMount() {
        dbfirebase.auth().onAuthStateChanged((user) => {
            if(user) {
                this.setState({
                    akun : user.email
                })
                this.props.logUser(user.email)
            }
        })
    }

    keluar = (e) => {
        e.preventDefault()
        dbfirebase.auth().signOut().then(()=> {
            this.setState({ akun: null })
        }).catch(() => {
            console.log('belum masuk')
        })
    }
    
    render() {
        return (
            <div className="container">
                <Navbar akun={this.state.akun} keluar={this.keluar} />
                <div className="my-5 pt-3">
                <Switch>
                    <Route exact path="/" render={()=><Home akun={this.state.akun} />} />
                    <Route path="/masuk" component={Login} />
                    <Route path="/daftar" component={SignUp} />
                    <Route path="/admin" component={Admin} />
                    <Route path="/koleksi" component={Koleksi} />
                    <Route path="/produk/:id" component={Produk} />
                </Switch>
                </div>
                <Footer />
            </div>
        );
    }
}

export default withRouter(connect(null, {logUser})(App));