import React, { Component } from 'react';
import { connect } from 'react-redux';
import { goalRef } from '../firebase'

function mapStateToProps(state) {
    return {

    };
}

class GoalList extends Component {
    constructor(props) {
        super()
        this.state = {
            goals: []
        }
    }

    componentDidMount() {
        goalRef.on( 'value', snap => {
            let goals = []
            snap.forEach(goal => {
                goals.push({email : goal.val().email,
                            pesan : goal.val().pesan})
            })
            this.setState({ goals })
        })
    }

    render() {
        return (
            <div>
                {this.state.goals.map(data => {
                    return(
                        <li className="list-group-item">{data.email} : {data.pesan}</li>
                    )
                })}
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
)(GoalList);