import React, { Component } from 'react';
import { goalRef } from '../firebase';
import { connect } from 'react-redux';

class AddGoal extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pesan: ''
        }

        this.inputPesan = this.inputPesan.bind(this)
        this.kirimPesan = this.kirimPesan.bind(this)
    }

    inputPesan(e) {
        this.setState({
            pesan : e.target.value
        })
    }

    kirimPesan(e) {
        e.preventDefault()
        goalRef.push({
            email: this.props.email,
            pesan: this.state.pesan
        })
        this.setState({
            pesan: ''
        })
    }

    render() {
        return (
            <div className="form-inline">
                <div className="form-group">
                    <input type="text"
                    placeholder="masukan"
                    className="form-control"
                    onChange={this.inputPesan}
                    />
                    <button
                    className="btn btn-succes"
                    type="button"
                    onClick={this.kirimPesan}
                    >Kirim</button>
                </div>
                
            </div>
        );
    }
}

function mapStateToProps(state) {
    return state
}

export default connect(mapStateToProps,null)(AddGoal);