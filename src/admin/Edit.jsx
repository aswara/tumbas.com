import React, { Component } from 'react';
import { dbProduk, dbGambar } from '../firebase';

class Edit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            progress: 0,
            statusUpload: '',
            lokasi: null,
        }
    }


    inputData = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    inputGambar = (e) => {
        const file = e.target.files[0];
        const nama = Date.now();
        const storageGambar = dbGambar.child(nama+file.name);
        const prosesSimpan = storageGambar.put(file);
        prosesSimpan.on('state_changed', (data)=>{
            console.log(data)
            const lokasi = data.ref.name
            const progress = (data.bytesTransferred/data.totalBytes) * 100
            this.setState({
                progress, lokasi

            })
        }, (err)=>{ this.setState({ statusUpload : 'gagal upload'}) },
         (complete)=>{ 
            prosesSimpan.snapshot.ref.getDownloadURL().then((url)=>{
               this.setState({ gambar: url, statusUpload : 'berhasil upload' })
            })
         }
        
        )}

    kirimData = (e) => {
        e.preventDefault()
        const { nama, harga, gambar, kategori, deskripsi } = this.state;
        let date = new Date();
        let tanggal = date.getDate() +'-'+ date.getMonth() +'-'+ date.getFullYear();
        if (nama && kategori && gambar && deskripsi && harga){ return(
        dbProduk.push({
            nama, harga, tanggal, gambar, kategori, deskripsi,
        }).then( alert('berhasil tambah') ) )} else {
            alert('tidak boleh ada yang kosong')
        }
    }

    hapusGambar = ()=> {     
        const lokGambar = dbGambar.child(this.state.lokasi)
        lokGambar.delete().then(()=>{this.setState({
            lokasi: null, statusUpload: 'berhasil hapus', gambar: null, progress: 0
        })})
    }

    render() {
        console.log(this.e)
        return (
            <div>

                <div className="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalCenterTitle">Modal title</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                    {
                   (this.state.lokasi) ? 
                   ( <div><button className="btn btn-danger" onClick={this.hapusGambar}>Ganti</button></div> ) : 
                   ( <div className="">
                   <label htmlFor="exampleFormControlFile1">Masukkan gambar produk</label>
                   <input onChange={this.inputGambar} type="file" class="form-control-file" name="gambar" id="exampleFormControlFile1" />
                     </div> )
                }
                <progress className="" value={this.state.progress} max="100"></progress>
                   <p>{this.state.statusUpload}</p>
                   <img src={this.state.gambar} alt=""/>
                <form onSubmit={this.kirimData}>
                    <div className="form-group row">
                        <label htmlFor="exampleFormControlInput1">Nama Produk</label>
                        <input required onChange={this.inputData} type="text" name="nama" className="form-control" id="exampleFormControlInput1" placeholder="masukan nama" />
                    </div>
                    <div className="form-group row">
                        <label required htmlFor="exampleFormControlInput1">Harga</label>
                        <input required onChange={this.inputData} type="text" name="harga" className="form-control" id="exampleFormControlInput1" placeholder="masukan harga" />
                    </div>
                    <div className="form-group row">
                        <label htmlFor="exampleFormControlSelect1">Kategori</label>
                        <select required onChange={this.inputData} className="form-control" name="kategori" id="exampleFormControlSelect1">
                        <option>Pakaian Anak</option>
                        <option>Pakaian Muslim</option>
                        <option>Pakaian Pria</option>
                        <option>Pakaian Wanita</option>
                        <option>Lain-lain</option>
                        </select>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="exampleFormControlTextarea1">Deskripsi</label>
                        <textarea required onChange={this.inputData} className="form-control" name="deskripsi" id="exampleFormControlTextarea1" rows="2"></textarea>
                    </div>
                    <button type="submit" className="btn btn-primary">Simpan</button>
                    </form>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" className="btn btn-primary">Save changes</button>
                    </div>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default Edit;