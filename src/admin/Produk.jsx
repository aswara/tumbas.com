import React, { Component } from 'react';
import Edit from './Edit';
import { dbProduk, dbGambar } from '../firebase';


class Produk extends Component {
    constructor(props) {
        super(props)
        this.state = {
            progress: 0,
            statusUpload: '',
            lokasi: null,
        }
    }


    inputData = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    inputGambar = (e) => {
        const file = e.target.files[0];
        const nama = Date.now();
        const storageGambar = dbGambar.child(nama+file.name);
        const prosesSimpan = storageGambar.put(file);
        prosesSimpan.on('state_changed', (data)=>{
            console.log(data)
            const lokasi = data.ref.name
            const progress = (data.bytesTransferred/data.totalBytes) * 100
            this.setState({
                progress, lokasi

            })
        }, (err)=>{ this.setState({ statusUpload : 'gagal upload'}) },
         (complete)=>{ 
            prosesSimpan.snapshot.ref.getDownloadURL().then((url)=>{
               this.setState({ gambar: url, statusUpload : 'berhasil upload' })
            })
         }
        
        )}

    kirimData = (e) => {
        e.preventDefault()
        const { key, nama, harga, gambar, kategori, deskripsi } = this.state;
        let date = new Date();
        let tanggal = date.getDate() +'-'+ date.getMonth() +'-'+ date.getFullYear();
        if (nama && kategori && gambar && deskripsi && harga){ return(
        dbProduk.child(key).update({
            nama, harga, tanggal, gambar, kategori, deskripsi,
        }).then( this.setState({ ubah:false }) ) )} else {
            alert('tidak boleh ada yang kosong')
        }
    }

    hapusGambar = ()=> {     
        const lokGambar = dbGambar.child(this.state.lokasi)
        lokGambar.delete().then(()=>{this.setState({
            lokasi: null, statusUpload: 'berhasil hapus', gambar: null, progress: 0
        })})
    }

    hapus = (key, gambar) => {
        this.props.hapus(key, gambar)
    }

    editData (produk) {
        this.setState(produk)
        this.setState({
            ubah: true
        })
    }


    render() {
        const { tanggal, nama, kategori, gambar, deskripsi, harga, key } = this.props.produk
        if(this.state.ubah) {
            return (
                <div className="card">
                {
                   (this.state.lokasi) ? 
                   ( <div><button className="btn btn-danger" onClick={this.hapusGambar}>Ganti</button></div> ) : 
                   ( <div className="">
                   <label htmlFor="exampleFormControlFile1">Masukkan gambar produk</label>
                   <input onChange={this.inputGambar} type="file" class="form-control-file" name="gambar" id="exampleFormControlFile1" />
                     </div> )
                }
                <progress className="" value={this.state.progress} max="100"></progress>
                   <p>{this.state.statusUpload}</p>
                   <img className="img-fluid" src={this.state.gambar} alt=""/>
                 <div className="card-body">
                     <input name="nama" onChange={this.inputData} className="form-control" value={this.state.nama} type="text"/>
                     <input name="harga" className="form-control" onChange={this.inputData} value={this.state.harga} type="text"/>
                     <select value={this.state.kategori} onChange={this.inputData} className="form-control" name="kategori" id="exampleFormControlSelect1">
                        <option>Pakaian Anak</option>
                        <option>Pakaian Muslim</option>
                        <option>Pakaian Pria</option>
                        <option>Pakaian Wanita</option>
                        <option>Lain-lain</option>
                    </select>
                     <textarea name="deskripsi" onChange={this.inputData} className="form-control" value={this.state.deskripsi} type="text"></textarea>
                     <button type="su" onClick={this.kirimData} className="btn btn-primary">Simpan</button>
                 </div>
                 
             </div>
            )
        }
        return (
            
               <div className="card">
                <img className="card-img-top" src={gambar} alt="Card image cap" />
                <div className="card-body">
                <h5 className="text-primary list-group-item">{nama}</h5>
                    <h6 className="list-group-item" >{kategori}</h6>
                    <h6 className="list-group-item" ><b>Rp {harga}</b></h6>
                    <h6 className="list-group-item" >{tanggal}</h6>
                    <h6 className="list-group-item" >{deskripsi}</h6>
                    <button onClick={()=>{this.hapus(key, gambar)}} className="btn btn-danger mr-2">Hapus</button>
                    <button onClick={()=>{this.editData(this.props.produk)}} className="btn btn-primary">Edit</button>
                </div>
                
            </div>
        );
    }
}

export default Produk;
