import React, { Component } from 'react';
import { dbProduk, dbGambar } from '../firebase';
import Loading from '../components/Loading';
import Produk from './Produk';
import {Link} from 'react-router-dom'; 

class Koleksi extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading: true,
            produk: [],
        }
    }
    componentDidMount(){
        dbProduk.on('value', (data)=>{
            
            const produk = [];
            data.forEach(item=>{
                const key = item.key
                const { gambar, nama, harga, kategori, tanggal, deskripsi } = item.val()
                produk.push({
                    key, gambar, nama, harga, kategori, tanggal, deskripsi        
                })
            })
            this.setState({ produk, loading: false })
        })
    }

    hapusProduk = (key, gambar) => {
        dbProduk.child(key).remove()

        const lokGambar = dbGambar.child(gambar)
        lokGambar.delete()
    }

    render() {
        if (this.state.loading) { return <Loading /> }
        return (
            <div>
                <h3>Koleksi</h3>
                <div className="row">
                { this.state.produk.map( item =>{
                    return(
                        <div className="col-md-3 my-2">
                        <Produk produk={item} hapus={this.hapusProduk} />             
                        </div>
                    )
                } ) }
                </div>
            </div>
        );
    }
}

export default Koleksi;