import React, { Component } from 'react';
import { dbUsers, dbfirebase } from '../firebase';

class User extends Component {
    constructor(){
        super()
        this.state = {
            email: '',
            nama: '',
            produk: [],
            loading: false
        }
    }

    componentDidMount() {
        dbfirebase.auth().onAuthStateChanged((user) => {
            if(user) {
                dbUsers.child(user.uid).child('keranjang').on('value', (data)=>{ 
                    const produk = [];
                    var total = 0;
                    data.forEach(item=>{
                        const key = item.key
                        const { gambar, nama, harga, kategori, tanggal, deskripsi } = item.val()
                        total += parseInt(harga);
                        produk.push({
                            key, gambar, nama, harga, kategori, tanggal, deskripsi        
                        })
                        console.log(total)
                    })
                    this.setState({ produk, loading: false, total })
                    
                })
                this.setState({
                    email : user.email,
                    nama : user.displayName,
                    uid : user.uid
                })
            }
        })    
    }

    hapus(key) {
        dbUsers.child(this.state.uid).child('keranjang').child(key).remove()
    }

    render() {
        console.log(this.state)
        return (
            <div className="row">
                <div className="col-md-3 text-center">
                    <img className="img-thumbnail card-img-top" src="https://image.flaticon.com/icons/svg/417/417777.svg" alt="Card image cap" />
                    <div className="card-body border">
                        <h5 className="text-primary">{this.state.nama}</h5>
                        <span>{this.state.email}</span>
                    </div>
                </div>
                <div className="col-md-9 border py-2 shadow">
                <div className="">
                 <h4>Keranjang</h4>
                 <div className="row">
                 { 
                    this.state.produk.map((item)=>{
                        return(
                            <div className="col-md-3 mt-2">
                            <img className="card-img-top" src={item.gambar} alt="Card image cap" />
                            
                                <h5 className="text-primary">{item.nama}</h5>
                                <h6>Rp {item.harga}</h6>
                                <h6>{item.kategori}</h6>
                                <button onClick={()=>{this.hapus(item.key)}} className="btn btn-danger mr-2">Hapus</button>                            
                        </div> 
                        )
                    })
                  }
                  </div>
                  <div className="row mt-3">
                    <div className="col-sm-6 py-2">
                        <h3>Total : Rp {this.state.total}</h3>
                    </div>
                    <div className="col-sm-6 text-right">
                    <button className="btn btn-outline-primary btn-lg">Lanjut Pembayaran</button>
                    </div>
                  </div>
            </div>
                </div>
                
            </div>
        );
    }
}

export default User;