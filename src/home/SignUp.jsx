import React, { Component } from 'react';
import { dbfirebase, dbUsers } from '../firebase';
import Loading from '../components/Loading';
import { Link } from 'react-router-dom'


class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            no: '',
            email:'',
            password: '',
            password2: '',
            loading: false
        }

        this.inputData = this.inputData.bind(this)
        
    }

    inputData(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    daftar = (e) => {
        e.preventDefault()
        this.setState({ loading: true })
        const { email, password, password2, username, no } = this.state;
        (password === password2)?(
        dbfirebase.auth().createUserWithEmailAndPassword(email, password)
        .then((users)=>{
            if(users !== null ){
                const akun = dbfirebase.auth().currentUser;
                akun.updateProfile({
                displayName: username,
                photoURL: "",
                }).then(function() {
                console.log("berhasil update akun")
                }).catch(function(error) {
                error
                });
               dbUsers.child(users.user.uid).set({
                    username, no, email
                }).then(  this.onLogin() )
            }
           
        })
        .catch(error =>{
            this.setState({
                pesan : error.message,
                loading: false
            })
        })):( this.setState({ pesan: 'password tidak sama', loading:false }) )
    }

    onLogin() {
        this.props.history.push('/');
    }

    render() {
        if(this.state.loading){ return <Loading /> }
        return (
            <div className="login shadow bg-shadow text-center">
                <div className="logologin bg-primary">
                <h2>Buat Akun</h2>
                </div>
                <div className="form-group px-5 mb-3">
                <p>{this.state.pesan}</p>
                    <input type="text" className="form-control shadow" value={this.state.username} placeholder="Username" name="username" onChange={this.inputData} />
                    <input type="text" className="form-control shadow" value={this.state.no} placeholder="no HP" name="no" onChange={this.inputData} />
                    <input type="email" className="form-control shadow" value={this.state.email} placeholder="Email" name="email" onChange={this.inputData} />
                    <input type="password" className="form-control shadow" value={this.state.password} placeholder="Password" name="password" onChange={this.inputData} />
                    <input type="password" className="form-control shadow" value={this.state.password2} placeholder="Ulangi password" name="password2" onChange={this.inputData} />
                    <button className="btn btn-outline-primary btn-block shadow" onClick={this.daftar}>Daftar</button>
                    <div>Sudah punya akun? <Link to='/masuk'>Masuk</Link></div>
                </div>
            </div>
        );
        
    }
}

export default SignUp;