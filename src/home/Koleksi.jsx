import React, { Component } from 'react';
import { dbProduk } from '../firebase';
import Loading from '../components/Loading';
import Produk from './ProdukKoleksi';
import {Link} from 'react-router-dom'; 

class Koleksi extends Component {
    constructor(props){
        super(props)
        this.state = {
            loading: true,
            produk: [],
        }
    }
    componentDidMount(){
        dbProduk.on('value', (data)=>{
            
            const produk = [];
            data.forEach(item=>{
                const key = item.key
                const { gambar, nama, harga, kategori, tanggal, deskripsi } = item.val()
                produk.push({
                    key, gambar, nama, harga, kategori, tanggal, deskripsi        
                })
            })
            this.setState({ produk, loading: false })
        })
    }

    hapusProduk = (key) => {
        dbProduk.child(key).remove()
    }

    render() {
        if (this.state.loading) { return <Loading /> }
        return (
            <div>
                <h3 className="text-primary">Koleksi</h3>
                <div className="row border shadow">
                { this.state.produk.map( item =>{
                    return(
                        <div className="col-md-3 my-3">
                        <Link to={{
                            pathname: `/produk/${item.nama}`,
                            state: { produk: item }
                        }}> <Produk produk={item} hapus={this.hapusProduk} /> </Link>
                        
                        </div>
                    )
                } ) }
                </div>
            </div>
        );
    }
}

export default Koleksi;