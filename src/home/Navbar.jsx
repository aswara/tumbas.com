import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { dbfirebase } from '../firebase';


class Navbar extends Component {
    constructor(props){
        super(props)
        this.state = {
            akun: null
        }
    }

    componentDidMount(){
        dbfirebase.auth().onAuthStateChanged((user)=>{
            this.setState({ akun: user.email })
        })
    }

    keluar = (e) => {
        e.preventDefault()
        this.props.keluar()
    }
    
    render() {

        return (
                <nav class="navbar navbar-light border fixed-top container">
                <a class="navbar-brand"><Link to='/'>Home</Link></a>
                <form class="form-inline">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" />
                    <Link to='/koleksi'><button class="btn btn-outline-success my-2 mx-2 my-sm-0" type="submit">Koleksi</button></Link>
                    { (!this.props.akun) ? ( <div>
                        <Link to='/daftar'><button class="btn btn-outline-success my-2 mx-2 my-sm-0" type="submit">Daftar</button></Link>
                        <Link to='/masuk'><button class="btn btn-outline-success my-2 mx-2 my-sm-0" type="submit">Masuk</button></Link>
                        </div> 
                    ) : <div>
                        <Link to='/'><button class="btn btn-outline-success my-2 mx-2 my-sm-0" type="submit">Keranjang</button></Link>
                        <button onClick={this.props.keluar} class="btn btn-outline-success my-2 mx-2 my-sm-0" type="submit">Keluar</button>
                        </div>
                     }             
                </form>
                </nav>
        );
    }
}

export default Navbar;