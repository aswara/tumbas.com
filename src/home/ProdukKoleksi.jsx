import React, { Component } from 'react';
import { dbProduk, dbGambar } from '../firebase';


class Produk extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    render() {
        const { tanggal, nama, kategori, gambar, deskripsi, harga, key } = this.props.produk
       
        return (
            
               <div className="card shadow">
                <img className="card-img-top" src={gambar} alt="Card image cap" />
                <div className="">
                    <h5 className="text-primary list-group-item">{nama}</h5>
                    <h6 className="list-group-item" >{kategori}</h6>
                    <h6 className="list-group-item" ><b>Rp {harga}</b></h6>
                </div>
                
            </div>
        );
    }
}

export default Produk;
