import React, { Component } from 'react';
import {dbUsers, dbfirebase} from '../firebase';

class Produk extends Component {
    state={
        akun:''
    }
    componentDidMount(){
        dbfirebase.auth().onAuthStateChanged((user)=>{
            if(user) {
                this.setState({ akun: user.uid })
            }

        })
    }
    
    keranjang =()=> {
        const { tanggal, nama, kategori, gambar, deskripsi, harga } = this.props.location.state.produk
        if(this.state.akun){
        dbUsers.child(this.state.akun).child('keranjang').push({
            tanggal, nama, kategori, gambar, deskripsi, harga
        }).then( alert('berhasil ditambahkan') )
        } else { alert('harus login') }
    }
    
    render() {
        console.log(this.state)
        const { tanggal, nama, kategori, gambar, deskripsi, harga, key } = this.props.location.state.produk
        return (
            <div>
                    <div className="row mb-5 produk">
                    <div className="col-md-5 offset-md-1 py-3">
                    <img className="img-thumbnail" src={gambar} alt="Card image cap" />
                    </div>
                    <div className="col-md-5">
                    <div className="text-primary card-body">
                        <h4 className="card-title list-group-item">{nama}</h4>
                        <h6 className="list-group-item">Harga : Rp {harga}</h6>
                        <h6 className="list-group-item">Tanggal upload : {tanggal}</h6>
                        <h6 className="list-group-item">Kategori : {kategori}</h6>
                        <p className="card-text list-group-item"><h5>Deskripsi :</h5> {deskripsi}</p>
                        <button onClick={this.keranjang} className="btn btn-block btn-primary">Masukan Keranjang</button>   
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Produk;