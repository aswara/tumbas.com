import React, { Component } from 'react';
import User from '../home/User';

class Home extends Component {
    constructor(props) {
        super()
    }

    
    render() {
        console.log(this.props.akun)
        return (
            <div>
                {
                    (this.props.akun)?
                    <User />
                    : (
                    <div>
                        <div class="jumbotron text-primary bg shadow">
                        <h1 class="display-4">Tumbas.com!</h1>
                        <p class="lead">Membangun bisnis mudah, murah dari rumah.</p>
                        <hr class="my-4" />
                        <p>Tidak peduli apakah Anda seorang pemula dalam bisnis online ataupun Anda seorang yang berpengalaman Bila tujuan Anda adalah Untuk Meningkatkan Omzet dan Profit Bisnis Anda Maka Anda berada di tempat yang tepat Sekarang.</p>
                        <a class="btn btn-outline-primary btn-lg shadow" href="#" role="button">Gabung Sekarang</a>
                        </div>
                        <div className="text-success text-center">
                            <h1>Fitur Lengkap dan Mudah Digunakan</h1>
                        </div> 
                    </div>
                    )
                }
            </div>
        );
    }
}

export default Home;