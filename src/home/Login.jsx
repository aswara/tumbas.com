 import React, { Component } from 'react';
import { dbfirebase } from '../firebase';
import { Link } from 'react-router-dom'
import Home from './Home';
import { Redirect } from 'react-router-dom';
import Loading from '../components/Loading';

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            pesan: false,
            email: '',
            password: '',
            loading: false,
            error: null
        }    
    }

    componentDidMount(){
        if(this.state.loading){
            this.masuk;
        }
    }

    inputData=(e)=> {
        this.setState({
            [e.target.name] : e.target.value
        })
    }

    masuk=(e)=> {
        this.setState({ loading : true })
        const { email, password } = this.state
        dbfirebase.auth().signInWithEmailAndPassword(email, password)
        .then(user => {
            this.setState({
                email: '',
                password: '',
            })
            this.onLogin(); 
        })
        .catch(error => {
            this.setState({
               loading: false,
               error: "email dan password salah silahkan coba lagi"
           }) 
        })
    }

    

    onLogin() {
        this.props.history.push('/');
    }

    render() {
        console.log(this.state.loading)
        if(this.state.loading){
            return(
                <Loading />
            )
        }

        return (
            <div className="login shadow text-center">
            <div className="logologin bg-primary">
                <h2>Silahkan Masuk</h2>
            </div>
                <div className="form-group px-5">
                    <p>{this.state.error}</p>
                    <input type="email" className="shadow form-control" value={this.state.email} placeholder="email" name="email" onChange={this.inputData} />
                    <input type="password" className="shadow form-control" value={this.state.password} placeholder="password" name="password" onChange={this.inputData} />
                    <button className="shadow btn btn-outline-primary btn-block" onClick={this.masuk}>Masuk</button>
                    <div>Belum punya akun? <Link to='/daftar'>Daftar</Link></div>
                </div>
            </div>
        );
    }
}

export default Login;
