import * as firebase from 'firebase';

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCklSCXMjb6Qrb3QhEBvwRlselO_SMC35o",
  authDomain: "ecommerce-7d7ad.firebaseapp.com",
  databaseURL: "https://ecommerce-7d7ad.firebaseio.com",
  projectId: "ecommerce-7d7ad",
  storageBucket: "ecommerce-7d7ad.appspot.com",
  messagingSenderId: "434696762619"
};
export const dbfirebase =  firebase.initializeApp(config);
export const dbProduk = firebase.database().ref('produk');
export const dbUsers = firebase.database().ref('users');
export const dbGambar = firebase.storage().ref('gambar/produk')
export const goalRef = firebase.database().ref('goal');

